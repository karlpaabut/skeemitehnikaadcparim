
#define aref_voltage 1.5        // we tie 3.3V to ARef and measure it with a multimeter!
int tempPin = A1;        
                      
                        
int tempReading;        
 
void setup(void) {
  // We'll send debugging information via the Serial monitor
  Serial.begin(9600);   
 
  // If you want to set the aref to something other than 5v
  analogReference(EXTERNAL);
}
 
 
void loop(void) {
 
  tempReading = analogRead(tempPin);  
 
  Serial.print("Temp reading = ");
  Serial.print(tempReading);     

  float voltage = tempReading * aref_voltage;
  voltage /= 1024.0; 
 
  // prindib andmed
  Serial.print(" - ");
  Serial.print(voltage); Serial.println(" volts");
 
  // now print out the temperature
  float temperatureC = (voltage - 0.5) * 100 ;
                                              
  Serial.print(temperatureC); Serial.println(" degrees C");
 
 
 
  delay(1000);
}
